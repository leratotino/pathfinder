import 'dart:async';

import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home:Splashscreen(),
    );
  }
}

class Splashscreen extends StatefulWidget {
  const Splashscreen({super.key});

  @override
  State<Splashscreen> createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  @override
  void initState() {
    // TODO: implement initState

  super.initState();
  Timer(Duration(seconds : 10), (){
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder:(_) => Welcome()));
    }
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 0, 10, 95),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("PathFinder", style: TextStyle(
              color: Colors.white,
              fontSize: 30,
              fontWeight: FontWeight.w800
            ),),
            CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white)
            )
          ], 
        ),
      ),

    );
  }
}

class Welcome extends StatelessWidget {
  const Welcome({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
         color: Color.fromARGB(255, 0, 10, 95) 
      ),
      child: Center(
        child: Stack(
          children: [
            Positioned(
              bottom: 320,
              left: 0,
              right: 0,

              child:Column(
                children: [
                  Image.asset('school.png',alignment: Alignment.center,),
                  
                ],
              ) ,),
              Positioned(
                bottom: 80,
                left: 15,
                right: 15,
                child: Container(
                  height: 50,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    

                  ),
                  child: Center(
                    child: Text('Get Started', style: TextStyle(
                      color: Color.fromARGB(255, 0, 10, 95),
                      fontSize: 18,
                      fontWeight: FontWeight.bold
                    ),
                    ))
                    ,
                )
                  
        )],
        ),
      ),
    ));
  }
}




import 'package:flutter/material.dart';

void main() {
   runApp(
    MaterialApp(
     home: Dashboard() ,
   )
   );
}

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(Icons.home, color: Colors.white, size: 50.0,),
                  Icon(Icons.menu,color: Colors.white, size: 50.0,)
                ],

              ),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Text("Dashboard Options",
                style: TextStyle(color:  Colors.white,
                fontSize: 28.0,
                fontWeight: FontWeight.bold
                ),textAlign: TextAlign.center,
                ),
                ),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Center(
                    child: Wrap(
                      spacing:20.0,
                      runSpacing: 20.0,
                      children: [
                        SizedBox(
                        width: 160.0,
                        height:160.0 , 
                        child: Card(
                          color: Color.fromARGB(255, 67, 67, 68),
                          elevation: 2.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                               children: [
                                Image.asset("assets/employee.png", width: 64.0,),
                                SizedBox(height: 10.0,),
                                Text("Careers",style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,


                                ),
                                ),
                                

                               ],
                              ),),
                          ),
                        ), 
                        ),

                         SizedBox(
                        width: 160.0,
                        height:160.0 , 
                        child: Card(
                          color: Color.fromARGB(255, 67, 67, 68),
                          elevation: 2.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                               children: [
                                Image.asset("assets/profits.png", width: 64.0,),
                                SizedBox(height: 10.0,),
                                Text("Careers In-Demand",textAlign: TextAlign.center,style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  


                                ),
                                ),
                                

                               ],
                              ),),
                          ),
                        ), 
                        ),

                        SizedBox(
                        width: 160.0,
                        height:160.0 , 
                        child: Card(
                          color: Color.fromARGB(255, 67, 67, 68),
                          elevation: 2.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                               children: [
                                Image.asset("assets/classroom.png", width: 64.0,),
                                SizedBox(height: 10.0,),
                                Text("Courses",style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,


                                ),
                                ),
                                

                               ],
                              ),),
                          ),
                        ), 
                        ),

                        SizedBox(
                        width: 160.0,
                        height:160.0 , 
                        child: Card(
                          color: Color.fromARGB(255, 67, 67, 68),
                          elevation: 2.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                               children: [
                                Image.asset("assets/booking-online.png", width: 64.0,),
                                SizedBox(height: 10.0,),
                                Text("Appointment",style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,


                                ),
                                ),
                                

                               ],
                              ),),
                          ),
                        ), 
                        )

                      ],
                    ),
                  ),
                  )


          ],
        )
        ),
    );
  }
}